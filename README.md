# HOTREE

> Form for creating and publishing an event

Form features:

* Title and Description
* Category
* Payment
* Reward points
* Coordinator
* Date and duration

## Built With

### JavaScript

* [Vue](https://vuejs.org/) - JavaScript Framework
* [Webpack](https://webpack.js.org/) - Module bundler
* [vuelidate](https://monterail.github.io/vuelidate/) - Vue.js model validation library
* [date-fns](https://date-fns.org/) - JavaScript date utility library

### HTML

* [Pug](https://pugjs.org/api/getting-started.html) - clean HTML syntax

### CSS

* [Sass](https://sass-lang.com/) - css extension

### Style

* [Prettier](https://prettier.io/) - code style formatter

## Requirements

For development, you will need [Node.js](https://nodejs.org/en/) installed on your environment.

## Installation

1. Clone the repo: `$ git clone https://gitlab.com/RoHell/hotree.git`
2. Go to project directory: `$ cd hotree`
3. Run: `$ npm install` to download dependencies

## Scripts

* serve - `$ npm run serve`
* build - `$ npm run build`
* lint - `$ npm run lint`

## Contributing

1. Fork it!
2. Create your feature branch: `$ git checkout -b my-new-feature`
3. Commit your changes: `$ git commit -am 'Add some feature'`
4. Push to the branch: `$ git push origin my-new-feature`
5. Submit a pull request :D
