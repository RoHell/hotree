export const paymentType = [
  { id: "free", label: "Free event", paid: false },
  { id: "paid", label: "Paid event", paid: true }
];

export const timeType = [
  { id: "am", label: "AM", am: true },
  { id: "pm", label: "PM", am: false }
];
