export default {
  props: {
    /**
     * v-model value
     */
    value: {
      type: [String, Number],
      default: null
    },
    /**
     * Sets input in error state
     */
    isError: {
      type: Boolean,
      default: false
    },
    /**
     * Sets hint text under input field
     */
    hint: {
      type: String,
      default: ""
    },
    /**
     * Sets input field width
     */
    width: {
      type: [String, Number],
      default: "100%"
    },
    /**
     * Sets description at input right side
     */
    description: {
      type: String,
      default: ""
    }
  },
  inheritAttrs: false,
  computed: {
    inputWidth() {
      return typeof this.width === "number" ? `${this.width}px` : this.width;
    },
    listeners() {
      const { input, ...listeners } = this.$listeners;
      return listeners;
    }
  },
  methods: {
    updateInputValue() {
      this.$emit("input", this.$refs.input.value);
    }
  }
};
