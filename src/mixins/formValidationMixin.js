import { validationMixin } from "vuelidate";
import { required } from "vuelidate/lib/validators";
import { isDate, isValid, isFuture, parse } from "date-fns";

export default {
  mixins: [validationMixin],
  validations: {
    title: {
      required
    },
    description: {
      required
    },
    coordinator: {
      email: {
        isEmailCorrect(value) {
          return !value || this.isCorrectPattern;
        }
      }
    },
    eventFee: {
      isFeeEntered() {
        return !!this.isEventFeeProvided;
      },
      isFeeNumber() {
        return this.isEventFeeNumber;
      }
    },
    reward: {
      isRewardNumber(value) {
        return !isNaN(value);
      }
    },
    duration: {
      isDurationNumber(value) {
        return !isNaN(value);
      }
    },
    date: {
      isDateValid() {
        return this.isDateValidated;
      },
      isDatePast() {
        return this.isDateFuture;
      }
    }
  },
  computed: {
    isCorrectPattern() {
      const emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return emailPattern.test(this.coordinator.email);
    },
    parsedDate() {
      return parse(this.date);
    },
    isDateValidated() {
      return isDate(this.parsedDate) && isValid(this.parsedDate);
    },
    isDateFuture() {
      return this.isDateValidated && isFuture(this.parsedDate);
    },
    isEventFeeProvided() {
      return !this.payment || (this.payment && this.eventFee);
    },
    isEventFeeNumber() {
      return this.isEventFeeProvided && !isNaN(this.eventFee);
    },
    errorMessages() {
      return {
        title:
          this.$v.$dirty && !this.$v.title.required
            ? "Title cannot be empty"
            : "",
        description:
          this.$v.$dirty && !this.$v.description.required
            ? "Description cannot be empty"
            : "",
        payment: this.paymentErrorMessage,
        reward:
          this.$v.$dirty && !this.$v.reward.isRewardNumber
            ? "Reward must be a number"
            : "",
        email:
          this.$v.$dirty && !this.$v.coordinator.email.isEmailCorrect
            ? "Incorrect email format"
            : "",
        duration:
          this.$v.$dirty && !this.$v.duration.isDurationNumber
            ? "Duration must be a number"
            : "",
        date: this.dateErrorMessage
      };
    },
    paymentErrorMessage() {
      let message = "";
      if (this.$v.$dirty) {
        if (!this.$v.eventFee.isFeeEntered) {
          message = "Fee cannot be empty";
        } else if (!this.$v.eventFee.isFeeNumber) {
          message = "Fee must be a number";
        }
      } else {
        message = "";
      }
      return message;
    },
    dateErrorMessage() {
      let message = "";
      if (this.$v.$dirty) {
        if (!this.$v.date.isDateValid) {
          message = "Fill missing fields";
        } else if (!this.$v.date.isDatePast) {
          message = "Start date can not be past";
        }
      } else {
        message = "";
      }
      return message;
    }
  }
};
